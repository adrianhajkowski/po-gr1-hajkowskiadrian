package pl.edu.uwm.wmii.hajkowskiadrian.laboratorium00;

public class Zadanie11 {
    public static void main(String[] args){
        System.out.println("Póki córeczki opiewałem wdzięki:\n" +
                "\n" +
                "Mamunia słucha, stryj czyta;\n" +
                "\n" +
                "Lecz skórom westchnął do serca i ręki:\n" +
                "\n" +
                "Ja słucham, cały dom pyta.\n" +
                "\n" +
                " \n" +
                "\n" +
                "Mama o wioskach i o duszach gada,\n" +
                "\n" +
                "Pan stryj o rangach, dochodach;\n" +
                "\n" +
                "A pokojowa służącego bada\n" +
                "\n" +
                "O mych w kochaniu przygodach.\n" +
                "\n" +
                " \n" +
                "\n" +
                "Mamo, stryjaszku! Jedną tylko duszę\n" +
                "\n" +
                "I na Parnasie mam włości;\n" +
                "\n" +
                "Dochodów piórem dorabiać się muszę,\n" +
                "\n" +
                "A ranga — u potomności.\n" +
                "\n" +
                " \n" +
                "\n" +
                "Czym dawniej kochał? Ciekawość jałowa!\n" +
                "\n" +
                "Czy kochać mogę? Dowiodę:\n" +
                "\n" +
                "Porzuć lokaja, kotko pokojowa,\n" +
                "\n" +
                "Przydź w wieczór na mą gospodę.");
    }
}
