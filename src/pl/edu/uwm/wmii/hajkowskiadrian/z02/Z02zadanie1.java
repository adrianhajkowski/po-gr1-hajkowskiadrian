package pl.edu.uwm.wmii.hajkowskiadrian.z01;

import java.util.Random;
import java.util.Scanner;

public class Z02zadanie1 {

    public static void main(String[] args) {
        Scanner odczyt = new Scanner(System.in);
        int n = 0;
        while (n < 1 || n > 100) {
            System.out.print("Podaj ilosc elementow w tbalicy : ");
            n = odczyt.nextInt();
        }
        int[] tab = new int[n];
        Random generator = new Random();
        for (int i = 0; i < n; i++) {
            tab[i] = generator.nextInt(1999) - 999;
        }
        int npar = 0;
        int par = 0;
        for (int i : tab) {
            if (i % 2 == 0)
                par++;
            else
                npar++;
        }
        System.out.println("Ilosc parzystych elemenetow = " + par +
                "Ilosc nieparzystych elemenetow = " + npar);
        int dod = 0;
        int ujem = 0;
        int zer = 0;
        for (int i : tab) {
            if (i > 0)
                dod++;
            if (i < 0)
                ujem++;
            if (i == 0)
                zer++;
        }
        System.out.println("Ilosc elementow dodatnich : " + dod +
                "\nIlosc elementow ujemnych : " + ujem +
                "\nIlosc elementow zerowych : " + zer);
        int najw = tab[0];
        int najm = tab[0];
        int najwf = 0;
        int najmf = 0;
        for (int i : tab) {
            if (najw < i)
                najw = i;
            if (najm > i)
                najm = i;
        }
        for (int i : tab) {
            if (i == najw)
                najwf++;
            if (i == najm)
                najmf++;
        }
        System.out.println("Najwiekszy element ma wartosc : " + najw + " wsytapil " + najwf + " razy.");
        System.out.println("Najmniejszy element ma wartosc : " + najm + " wystapil " + najmf + " razy.");
        int sdod = 0;
        int sujem = 0;
        for (int i : tab) {
            if (i > 0)
                sdod += i;
            if (i < 0)
                sujem += i;
        }
        System.out.println("Suma elementow ujemnych : " + sujem + "\nSuma elementow dodatnich : " + sdod);
        int temp = 0;
        int najd = 0;
        for (int i : tab) {
            if (i > 0) {
                temp++;
            }
            if (i < 0) {
                if (najd < temp)
                    najd = temp;
                temp = 0;
            }
        }
        if (najd < temp)
            najd = temp;
        System.out.println("Najdluzszy ciag dodatnich zankow wynosi : " + najd);
        for (int i = 0; i < n; i++) {
            if (tab[i] > 0)
                tab[i] = 1;
            if (tab[i] < 0)
                tab[i] = -1;
        }
        for (int i : tab)
            System.out.print(i + " ");
        System.out.println();
        int lewy = 0;
        while (lewy < 1 || lewy > 100) {
            System.out.print("Podaj lewy kraniec zmiany : ");
            lewy = odczyt.nextInt();
        }
        int prawy = 0;
        while (prawy < 1 || prawy > 100) {
            System.out.print("Podaj prawy kraniec zmiany : ");
            prawy = odczyt.nextInt();
        }
        if (lewy > prawy)
        {
            temp = lewy;
            lewy = prawy;
            prawy = temp;
        }
        int half = lewy + ((prawy + 1) - lewy) / 2;
        int endCount = prawy;
        for (int startCount = lewy; startCount < half; startCount++) {
            int store = tab[startCount];
            tab[startCount] = tab[endCount];
            tab[endCount] = store;
            endCount--;
        }
        for(int i : tab)
            System.out.print(i + " ");
    }
}
