package pl.edu.uwm.wmii.hajkowskiadrian.z02;

import java.util.Random;
import java.util.Scanner;

public class Z02zadanie3 {
    public static void main(String[] args)
    {
        int m,n,k;
        Scanner odczyt = new Scanner(System.in);
        do {
            System.out.print("Podaj m : ");
            m = odczyt.nextInt();
        }
        while(m<1&&m>10);
        do {
            System.out.print("Podaj n : ");
            n = odczyt.nextInt();
        }
        while(n<1&&n>10);
        do {
            System.out.print("Podaj k : ");
            k = odczyt.nextInt();
        }
        while(k<1&&k>10);
        int[][] a = new int[m][n];
        int[][] b = new int[n][k];
        int[][] c = new int[m][k];
        Random generator = new Random();
        for(int i=0;i<m;i++)
            for(int j=0;j<n;j++)
                a[i][j]=generator.nextInt(10);
        for(int i=0;i<n;i++)
            for(int j=0;j<k;j++)
                b[i][j]=generator.nextInt(10);
        System.out.println("Macierz a");
        for(int i=0;i<m;i++)
        {
            for(int j=0;j<n;j++)
                System.out.print(a[i][j]+" ");
            System.out.print("\n");
        }
        System.out.println("Macierz b");
        for(int i=0;i<n;i++)
        {
            for(int j=0;j<k;j++)
                System.out.print(b[i][j]+" ");
            System.out.print("\n");
        }
        int suma = 0;
        for(int i = 0; i < m; i++)
        {
            for(int j = 0; j < m; j++)
            {
                for(int l = 0; l < k ; l++)
                    suma += a[i][l] * b[l][j];

                c[i][j] = suma;
                suma = 0;
            }
        }
        System.out.println("Macierz c");
        for(int i=0;i<m;i++)
        {
            for(int j=0;j<k;j++)
                System.out.print(c[i][j]+" ");
            System.out.print("\n");
        }
    }
}
