package pl.edu.uwm.wmii.hajkowskiadrian.z01;

import java.util.Random;
import java.util.Scanner;

import static java.lang.Math.abs;

public class Z02zadanie2 {
    public static void generuj (int tab[],int n, int minWartosc, int maxWartosc)
    {
        Random generator = new Random();
        for (int i = 0; i < n; i++) {
            tab[i] = generator.nextInt(abs(maxWartosc-minWartosc)+1) + minWartosc;
        }
    }
    public static int ileNieparzystych(int tab[])
    {
        int i = 0;
        for(int j : tab)
        {
            if(j%2!=0)
                i++;
        }
        return i;
    }
    public static int ileParzystych(int tab[])
    {
        int i = 0;
        for(int j : tab)
        {
            if(j%2==0)
                i++;
        }
        return i;
    }
    public static int ileDodatnich(int tab[])
    {
        int i = 0;
        for(int j : tab)
        {
            if(j>0)
                i++;
        }
        return i;
    }
    public static int ileUjemnych(int tab[])
    {
        int i = 0;
        for(int j : tab)
        {
            if(j<0)
                i++;
        }
        return i;
    }
    public static int ileZerowych(int tab[])
    {
        int i = 0;
        for(int j : tab)
        {
            if(j==0)
                i++;
        }
        return i;
    }
    public static int ileMaksymalnych(int tab[])
    {
        int i = 0;
        int max = tab[0];
        for(int j : tab)
        {
            if(j>max)
                max = j;
        }
        for(int j : tab)
        {
            if(j==max)
                i++;
        }
        return i;
    }
    public static int sumaDodatnich(int tab[])
    {
        int sum = 0;
        for(int i : tab)
        {
            if(i>0)
                sum+=i;
        }
        return sum;
    }
    public static int sumaUjemnych(int tab[])
    {
        int sum = 0;
        for(int i : tab)
        {
            if(i>0)
                sum+=i;
        }
        return sum;
    }
    public static int dlugoscMaksymalnegoCiaguDodatnich(int tab[])
    {
        int temp = 0;
        int longest = 0;
        for(int i : tab)
        {
            if(i>0)
            {
                temp++;
            }
            else
            {
                if(temp>longest)
                {
                    longest = temp;
                }
                temp = 0;
            }
        }
        if(temp>longest)
        {
            longest = temp;
        }
        return longest;
    }
    public static void signum(int tab[])
    {
        for(int i : tab)
        {
            if(i>0)
                i=1;
            if(i<0)
                i=-1;
        }
    }
    public static void odwrocFragment(int tab[],int lewy, int prawy)
    {
        int half = lewy + ((prawy + 1) - lewy) / 2;
        int endCount = prawy;
        for (int startCount = lewy; startCount < half; startCount++) {
            int store = tab[startCount];
            tab[startCount] = tab[endCount];
            tab[endCount] = store;
            endCount--;
        }
    }
    public static void main(String[] args)
    {
        Scanner odczyt = new Scanner(System.in);
        System.out.print("Podaj ilosc elementow w tablicy : ");
        int n = odczyt.nextInt();
        int[] tab = new int [n];
        System.out.print("Podaj minimalna wartosc elementow : ");
        int min = odczyt.nextInt();
        System.out.print("Podaj maxymalna wartosc elementow : ");
        int max = odczyt.nextInt();
        generuj(tab,n,min,max);
        System.out.println("Ilosc nieparzystych elementow : "+ileNieparzystych(tab));
        System.out.println("Ilosc parzystych elementow : "+ileParzystych(tab));
        System.out.println("Ilosc elementow dodatnich : "+ileDodatnich(tab));
        System.out.println("Ilosc elementow ujemnych : "+ileUjemnych(tab));
        System.out.println("Ilosc elementow zerowych : "+ileZerowych(tab));
        System.out.println("Ilosc elementow o najwyzszej wartosci : "+ileMaksymalnych(tab));
        System.out.println("Suma dodatnich : "+sumaDodatnich(tab));
        System.out.println("Suma ujemnych : "+sumaUjemnych(tab));
        System.out.println("Najdluzszy ciag elementow dodatnich : "+dlugoscMaksymalnegoCiaguDodatnich(tab));
        signum(tab);
        System.out.print("Podaj lewy kraniec do odwrocenia fragmentu : ");
        int lewy = odczyt.nextInt();
        System.out.print("Podaj prawy kraniec do odwrocenia fragmentu : ");
        int prawy = odczyt.nextInt();
        if(lewy>prawy)
        {
            int temp = prawy;
            prawy = lewy;
            lewy = temp;
        }
        odwrocFragment(tab,lewy,prawy);
        for(int i : tab)
            System.out.print(i+" ");
    }
}
