package pl.edu.uwm.wmii.hajkowskiadrian.z05;

import java.net.Inet4Address;
import java.util.ArrayList;

public class Zadanie1 {
    public static ArrayList<Integer> append(ArrayList<Integer> a, ArrayList<Integer> b)
    {
        ArrayList<Integer> temp = new ArrayList<Integer>();
        for(int i = 0 ; i < a.size() ; i++)
        {
            temp.add(a.get(i));
        }
        for(int i = 0 ; i < b.size() ; i++)
        {
            temp.add(b.get(i));
        }
        return temp;
    }
    public static void main(String[] args)
    {
        ArrayList<Integer> a = new ArrayList<Integer>();
        a.add(-1);
        a.add(0);
        a.add(1);
        a.add(2);
        a.add(3);
        a.add(4);
        a.add(6);
        System.out.print("Tablica a : ");
        for(int i = 0 ; i < a.size() ; i ++)
            System.out.print(a.get(i) + " ");
        System.out.print("\n");
        ArrayList<Integer> b = new ArrayList<Integer>();
        b.add(5);
        b.add(7);
        b.add(2);
        b.add(3);
        b.add(1);
        b.add(4);
        b.add(5);
        System.out.print("Tablica b : ");
        for(int i = 0 ; i < b.size() ; i ++)
            System.out.print(b.get(i) + " ");
        System.out.print("\n");
        ArrayList<Integer> c = append(a,b);
        System.out.print("Tablica c : ");
        for(int i = 0 ; i < c.size() ; i ++)
            System.out.print(c.get(i) + " ");
        System.out.print("\n");
    }
}
