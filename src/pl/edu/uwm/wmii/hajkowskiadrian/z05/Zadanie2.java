package pl.edu.uwm.wmii.hajkowskiadrian.z05;

import java.util.ArrayList;

public class Zadanie2 {
    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b)
    {
        int n = 0;
        if(a.size()>=b.size())
        {
            n = a.size();
        }
        else
        {
            n = b.size();
        }
        ArrayList<Integer> temp = new ArrayList<Integer>();
        for(int i=0;i<n;i++)
        {
            if(i<a.size())
                temp.add(a.get(i));
            if(i<b.size())
                temp.add(b.get(i));
        }
        return temp;
    }
    public static void main (String args[])
    {
        ArrayList<Integer> a = new ArrayList<Integer>();
        ArrayList<Integer> b = new ArrayList<Integer>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);
        b.add(9);
        b.add(7);
        b.add(4);
        b.add(9);
        b.add(11);
        ArrayList<Integer> c = new ArrayList<Integer>();
        c = merge(a,b);
        System.out.print("A : ");
        for(int i=0;i<a.size();i++)
            System.out.print(a.get(i)+" ");
        System.out.print("\n");
        System.out.print("B : ");
        for(int i=0;i<b.size();i++)
            System.out.print(b.get(i)+" ");
        System.out.print("\n");
        System.out.print("C : ");
        for(int i=0;i<c.size();i++)
            System.out.print(c.get(i)+" ");
        System.out.print("\n");
    }
}
