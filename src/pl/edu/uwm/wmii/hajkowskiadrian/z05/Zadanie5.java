package pl.edu.uwm.wmii.hajkowskiadrian.z05;

import java.util.ArrayList;

public class Zadanie5 {
    public static void reverse(ArrayList<Integer> a)
    {
        int n = a.size()/2;
        int temp=0;
        for(int i=0;i<n;i++)
        {
            temp = a.get(i);
            a.set(i,a.get(a.size()-1-i));
            a.set(a.size()-1-i,temp);
        }
    }
    public static void main (String[] args)
    {
        ArrayList<Integer> a = new ArrayList<Integer>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);
        a.add(5);
        System.out.print("A : ");
        for(int i=0;i<a.size();i++)
            System.out.print(a.get(i)+" ");
        System.out.print("\n");
        reverse(a);
        System.out.print("A : ");
        for(int i=0;i<a.size();i++)
            System.out.print(a.get(i)+" ");
    }
}
