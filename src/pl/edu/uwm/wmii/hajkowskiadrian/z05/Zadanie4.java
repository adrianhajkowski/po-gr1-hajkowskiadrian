package pl.edu.uwm.wmii.hajkowskiadrian.z05;

import java.util.ArrayList;

public class Zadanie4 {
    public static ArrayList<Integer> reversed(ArrayList<Integer> a)
    {
        ArrayList<Integer> temp = new ArrayList<>();
        for(int i=a.size()-1;i>=0;i--)
            temp.add(a.get(i));
        return temp;
    }
    public static void main (String[] args)
    {
        ArrayList<Integer> a = new ArrayList<Integer>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);
        ArrayList<Integer> b = reversed(a);
        System.out.print("A : ");
        for(int i=0;i<a.size();i++)
            System.out.print(a.get(i)+" ");
        System.out.print("\n");
        System.out.print("B : ");
        for(int i=0;i<b.size();i++)
            System.out.print(b.get(i)+" ");
    }
}
