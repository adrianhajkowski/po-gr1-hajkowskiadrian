package pl.edu.uwm.wmii.hajkowskiadrian.z04;

import java.util.Scanner;

public class Zadanie1d {
    public static String repeat (String str, int n)
    {
        String wynik = "";
        for(int i = 0;i<n;i++)
        {
            wynik+=str;
        }
        return wynik;
    }
    public static void main(String[] args)
    {
        Scanner odczyt = new Scanner(System.in);
        System.out.println("Podaj napis : ");
        String napis = odczyt.nextLine();
        System.out.print("Podaj ilosc powtorzen : ");
        int n = odczyt.nextInt();
        System.out.println(repeat(napis,n));
    }
}
