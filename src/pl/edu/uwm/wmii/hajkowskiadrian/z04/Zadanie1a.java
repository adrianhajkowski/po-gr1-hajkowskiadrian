package pl.edu.uwm.wmii.hajkowskiadrian.z04;

import java.util.Scanner;

public class Zadanie1a {
    public static int countChar(String str, char c)
    {
        int l=0;
        for(int i=0;i<str.length();i++)
        {
            if(str.charAt(i)==c)
                l++;
        }
        return l;
    }
    public static void main(String[] args)
    {
        Scanner odczyt = new Scanner(System.in);
        System.out.println("Podaj napis : ");
        String napis = odczyt.nextLine();
        System.out.print("Podaj znak : ");
        char znak = odczyt.next().charAt(0);
        System.out.println("Znak "+znak+" wystepuje "+ countChar(napis,znak) +" razy.");
    }
}
