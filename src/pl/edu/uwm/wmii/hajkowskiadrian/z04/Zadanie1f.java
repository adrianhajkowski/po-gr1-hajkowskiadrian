package pl.edu.uwm.wmii.hajkowskiadrian.z04;

import java.util.Scanner;

public class Zadanie1f {
    public static String change(String str)
    {
        String temp = "";
        for(int i=0;i<str.length();i++)
        {
            if(Character.isLowerCase(str.charAt(i)))
                temp+=str.toUpperCase().charAt(i);
            if(Character.isUpperCase(str.charAt(i)))
                temp+=str.toLowerCase().charAt(i);
        }
        return temp;
    }
    public static void main(String[] args)
    {
        Scanner odczyt = new Scanner (System.in);
        System.out.println("Podaj napis : ");
        String napis = odczyt.nextLine();
        System.out.println(change(napis));
    }
}
