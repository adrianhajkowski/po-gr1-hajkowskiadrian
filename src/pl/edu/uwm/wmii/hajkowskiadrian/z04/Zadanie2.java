package pl.edu.uwm.wmii.hajkowskiadrian.z04;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Zadanie2 {
    public static void main(String adres, char znak) throws FileNotFoundException{
        File plik = new File(adres);
        Scanner odczyt = new Scanner(plik);
        String linia = new String();
        int j = 0;
        while(odczyt.hasNextLine())
        {
            linia = odczyt.nextLine();
            if(linia.length()>0)
            {
                for(int i = 0;i<linia.length();i++)
                {
                    if(znak == linia.charAt(i))
                        j++;
                }
            }
        }
        System.out.println("Znak znajduje sie "+j+" razy.");
    }
}
