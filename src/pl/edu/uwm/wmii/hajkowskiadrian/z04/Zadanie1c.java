package pl.edu.uwm.wmii.hajkowskiadrian.z04;

import java.util.Scanner;

public class Zadanie1c {
    public static String middle(String str)
    {
        int position = 0;
        int length = 0;
        if(str.length()%2==0)
        {
            position = str.length() / 2 - 1;
            length = 2;
        }
        if(str.length()%2==1)
        {
            position = str.length()/2;
            length = 1;
        }
        return str.substring(position, position + length);
    }
    public static void main(String[] args)
    {
        Scanner odczyt = new Scanner(System.in);
        System.out.println("Podaj napis : ");
        String napis = odczyt.nextLine();
        System.out.println("Srodkowy znak to : "+middle(napis));
    }
}
