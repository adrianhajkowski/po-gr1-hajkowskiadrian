package pl.edu.uwm.wmii.hajkowskiadrian.z04;

        import java.util.Scanner;

public class Zadanie1b {
    public static int countSubStr(String str, String subStr)
    {
        int counter = 0;
        int temp = 0;
        for(int i=0;i<str.length();i++)
        {
            if(temp==subStr.length())
            {
                counter++;
                temp=0;
            }
            if(str.charAt(i)==subStr.charAt(temp))
                temp++;
        }
        if(temp==subStr.length())
        {
            counter++;
            temp=0;
        }
        return counter;
    }
    public static void main(String[] args)
    {
        Scanner odczyt = new Scanner (System.in);
        System.out.println("Podaj napis : ");
        String napis = odczyt.nextLine();
        System.out.println("Podaj podnapis : ");
        String podanpis = odczyt.nextLine();
        System.out.println("Podnapis wystepuje "+countSubStr(napis,podanpis)+" razy.");
    }
}
