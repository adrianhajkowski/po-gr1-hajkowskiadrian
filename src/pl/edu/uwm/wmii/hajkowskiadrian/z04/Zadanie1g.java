package pl.edu.uwm.wmii.hajkowskiadrian.z04;

import java.util.Scanner;

public class Zadanie1g {
    public static String nice(String str)
    {
        StringBuffer nowy = new StringBuffer();
        int counter=0;
        for(int i=str.length()-1;i>=0;i--)
        {
            nowy.append(str.charAt(i));
            counter++;
            if(counter==3&&i!=0)
            {
                nowy.append(";");
                counter=0;
            }
        }
        nowy.reverse();
        return nowy.toString();
    }
    public static void main(String[] args) {
        Scanner odczyt = new Scanner(System.in);
        System.out.println("Podaj ciag liczb.");
        String ciag = odczyt.nextLine();
        System.out.print(nice(ciag));
    }
}
