package pl.edu.uwm.wmii.hajkowskiadrian.z04;

import java.util.Scanner;

public class Zadanie1e {
    public static int[] where(String str, String subStr)
    {
        int counter = 0;
        int temp = 0;
        for(int i=0;i<str.length();i++)
        {
            if(temp==subStr.length())
            {
                counter++;
                temp=0;
            }
            if(str.charAt(i)==subStr.charAt(temp))
                temp++;
        }
        if(temp==subStr.length())
        {
            counter++;
            temp=0;
        }
        int[] where = new int[counter];
        counter = 0;
        for(int i=0;i<str.length();i++)
        {
            if(temp==subStr.length())
            {
                where[counter]=i-temp;
                counter++;
                temp=0;
            }
            if(str.charAt(i)==subStr.charAt(temp))
                temp++;
        }
        if(temp==subStr.length())
        {
            where[counter]=str.length()-temp;
        }
        return where;
    }
    public static void main(String[] args)
    {
        Scanner odczyt = new Scanner (System.in);
        System.out.println("Podaj napis : ");
        String napis = odczyt.nextLine();
        System.out.println("Podaj podnapis : ");
        String podanpis = odczyt.nextLine();
        int[] tab = where(napis,podanpis);
        System.out.print("Podanpis zaczyna sie przy indeksach : ");
        for(int i : tab)
        {
            System.out.print(i+" ");
        }
    }
}
