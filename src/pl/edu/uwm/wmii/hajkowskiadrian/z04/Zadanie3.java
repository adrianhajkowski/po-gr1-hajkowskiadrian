package pl.edu.uwm.wmii.hajkowskiadrian.z04;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Zadanie3 {
    public static void main(String adres, String wyraz) throws FileNotFoundException{
        File plik = new File(adres);
        Scanner odczyt = new Scanner(plik);
        String linia = new String();
        int j = 0;
        while(odczyt.hasNextLine())
        {
            linia = odczyt.nextLine();
            if(linia.length()>0)
            {
                StringTokenizer st = new StringTokenizer(linia, " \t\n\r\f.,:;()[]\"'?!-{}");
                while(st.hasMoreTokens())
                {
                    String temp = st.nextToken();
                    if(temp.equals(wyraz))
                        j++;
                }
            }
        }
        System.out.println("Wyraz znajduje sie "+j+" razy.");
    }
}
