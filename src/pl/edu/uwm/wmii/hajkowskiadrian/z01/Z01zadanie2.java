package pl.edu.uwm.wmii.hajkowskiadrian.z01;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static java.lang.Math.abs;
import static java.lang.Math.pow;
import static java.lang.StrictMath.sqrt;

public class Z01zadanie2 {
    public static int silnia(int n)
    {
        if(n<2)
            return 1;
        else
            return n*silnia(n-1);
    }
    public static void main(String[] args)
    {
        System.out.print("Podaj liczbe elementow w tablicy : ");
        Scanner odczyt = new Scanner(System.in);
        int[] tab = new int[odczyt.nextInt()];
        for(int i = 0;i<tab.length;i++)
        {
            System.out.print("Podaj element nr "+i+" : ");
            tab[i] = odczyt.nextInt();
        }
        System.out.print("Podaj waraint programu od a do h : ");
        String choice = odczyt.next();
        List<Integer> lista = new ArrayList<Integer>();
        switch(choice){
             case "a":
                for(int i : tab)
                    if(i%2==1)
                        lista.add(i);
                    break;
            case "b":
                for(int i : tab)
                    if(i%3==0&&i%5==1)
                        lista.add(i);
                    break;
            case "c":
                for(int i : tab)
                    if(sqrt(i)%2==0)
                        lista.add(i);
                    break;
            case "d":
                for(int i=1;i<tab.length-1;i++)
                {
                    if(tab[i]<(tab[i-1]+tab[i+1])/2)
                        lista.add(tab[i]);
                }
                break;
            case "e":
                for(int i=0;i<tab.length;i++)
                {
                    if(pow(2.0,tab[i])<tab[i]&&tab[i]<silnia(i+1))
                        lista.add(tab[i]);
                }
                break;
            case "f":
                for(int i=0;i<tab.length;i++)
                {
                    if(i%2==0)
                        lista.add(tab[i]);
                }
                break;
            case "g":
                for(int i : tab)
                    if(!(i<0)&&i%2==1)
                        lista.add(i);
                    break;
            case "h":
                for(int i : tab)
                    if(abs(i)==pow(i+1,2));
                    break;
        }
        System.out.println("Elementy spelniajacy podany warunek : ");
        for(int i : lista)
            System.out.println(i);
        System.out.println("Druga czesc - Podaj ile elementow ma posiadac tablica : ");
        double[] tab2 = new double[odczyt.nextInt()];
        for(int i=0;i<tab2.length;i++)
        {
            System.out.println("Podaj element numer "+i+" : ");
            tab2[i]=odczyt.nextDouble();
        }
        double drug = 0;
        for(double i : tab2)
        {
            if(i%2==0)
            {
                drug+=i;
            }
        }
        drug *= 2.0;
        System.out.println("Wynik drugiej czesci to : " + drug);
        System.out.print("Trzecia czesc - Podaj ile liczb posiada tablica : ");
        double[] tab3 = new double[odczyt.nextInt()];
        int poz = 0;
        int neg = 0;
        int zer = 0;
        for(double i : tab3)
        {
            if(i>0)
                poz++;
            if(i<0)
                neg++;
            if(i==0)
                zer++;
        }
        System.out.println("Elementow dodatnich : "+poz);
        System.out.println("Elementow niedodatnich : "+neg);
        System.out.println("Elementow zerowych : "+zer);
        System.out.print("Czwarta czesc - Podaj ile liczb posiada tablica : ");
        double[] tab4 = new double[odczyt.nextInt()];
    }
}
