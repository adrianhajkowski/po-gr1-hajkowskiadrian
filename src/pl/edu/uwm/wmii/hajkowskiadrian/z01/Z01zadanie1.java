package pl.edu.uwm.wmii.hajkowskiadrian.z01;

import java.util.Scanner;

import static java.lang.Math.abs;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class Z01zadanie1 {

	public static int silnia(int n)
	{
		if(n<2)
			return 1;
		else
			return n*silnia(n-1);
	}

    public static void main(String[] args) {
	System.out.print("Podaj liczbe elementow w tablicy : ");
	int n;
	Scanner odczyt = new Scanner(System.in);
	n = odczyt.nextInt();
	double [] tab = new double[n];
	for(int i = 0;i<n;i++)
	{
		System.out.print("Podaj liczbe pod nr "+i+" : ");
		tab[i]=odczyt.nextDouble();
	}
	double a = 0;
	for(double i : tab)
	{
		a+=i;
	}
	double b = 1;
	for(double i : tab)
	{
		b*=i;
	}
	double c = 0;
	for(double i : tab)
	{
		c+=abs(i);
	}
	double d = 0;
	for(double i : tab)
	{
		d+=sqrt(abs(i));
	}
	double e = 1;
	for(double i : tab)
	{
		e*=abs(i);
	}
	double f = 0;
	for(double i : tab)
	{
		f+=pow(i,2.0);
	}
	double g = 1;
	for(double i : tab)
	{
		g*=i;
	}
	for(double i : tab)
	{
		g+=i;
	}
	double h = 0;
	for(int i = 0;i<n;i++)
	{
		h+=pow(-1.0,tab[i])*i;
	}
	double j = 0;
	for(int i = 0;i<n;i++)
	{
		j+=(pow(-1.0,i)*tab[i])/silnia(i+1);
	}
	System.out.println("Wartosc wyrazen : ");
	System.out.println("a = "+ a );
	System.out.println("b = "+ b );
	System.out.println("c = "+ c );
	System.out.println("d = "+ d );
	System.out.println("e = "+ e );
	System.out.println("f = "+ f );
	System.out.println("g = "+ g );
	System.out.println("h = "+ h );
	System.out.println("i = "+ j );
	System.out.print("Czesc druga - podaj liczbe elementow w talbicy : ");
	n = odczyt.nextInt();
	double [] tab2 = new double[n];
	for(int i=0;i<n;i++)
	{
		System.out.print("Podaj element nr "+i+" : ");
		tab2[i]=odczyt.nextDouble();
	}
	for(int i=1;i<n;i++)
	{
		System.out.println(tab[i]);
	}
	System.out.println(tab[0]);
    }
}
